import express from 'express';
import * as dotenv from 'dotenv';
import cors from 'cors';
import OpenAI from "openai";

dotenv.config();
const openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY,
});


const app = express();
app.use(cors());
app.use(express.json());
app.get('/', (req, res) => {
    res.status(200).send({
        message: 'Hello from Codex',
    })
});

app.post('/', async (req, res) => {
    try {
        const prompt = req.body.prompt;

        const response = await openai.chat.completions.create({
            model: "gpt-3.5-turbo",
            messages: [
                {
                    "role": "user",
                    "content": `${prompt}`
                }
            ],
            temperature: 0,
            max_tokens: 256,
            top_p: 1,
            frequency_penalty: 0.5,
            presence_penalty: 0,
        });

        const messages = response.choices[0].message;

        res.status(200).send({
            bot: response.choices[0].message.content,
        });
    } catch (error) {
        console.error(error);
        res.status(500).send({
            message: 'Something went wrong',
        });
    }
});

app.listen(5000, () => {
    console.log('Server is running on port 5000');
});

